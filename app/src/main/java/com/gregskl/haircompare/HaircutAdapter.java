package com.gregskl.haircompare;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

public class HaircutAdapter extends RecyclerView.Adapter<HaircutAdapter.ViewHolder>{

    private List<Haircut> haircuts;
    private SimpleDateFormat displayFormatter = (SimpleDateFormat) SimpleDateFormat.getDateInstance(DateFormat.MEDIUM);

    public HaircutAdapter(List<Haircut> haircuts) {
        this.haircuts = haircuts;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public HaircutAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.haircut, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Haircut h = haircuts.get(position);
        Context context = holder.before.getContext();
        RequestOptions options = new RequestOptions()
                .frame(0)
                .centerCrop();
        String beforePath = getMediaPath(h.getID(), "before");
        Glide.with(context)
                .load(beforePath)
                .apply(options)
                .into(holder.before);
        holder.before.setOnClickListener(getImageListener(beforePath));
        String afterPath = getMediaPath(h.getID(), "after");
        Glide.with(context)
                .load(afterPath)
                .apply(options)
                .into(holder.after);
        holder.after.setOnClickListener(getImageListener(afterPath));
        holder.date.setText(displayFormatter.format(h.getDate()));
        holder.hairdresser.setText(h.getHairdresser());
        holder.price.setText(String.valueOf(h.getPrice()));
        holder.currencyType.setText(h.getCurrencyType());
        if(h.getRating() != 0.0 ) {
            holder.rating.setText(formatRating(h.getRating()));
        }
        else {
            holder.rating.setVisibility(View.INVISIBLE);
            holder.ratingSeparator.setVisibility(View.INVISIBLE);
        }
        Drawable star = ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_star, null);
        star.setBounds(0, 0,
                context.getResources().getDimensionPixelSize(R.dimen.star_size),
                context.getResources().getDimensionPixelSize(R.dimen.star_size));
        holder.rating.setCompoundDrawables(null, null, star, null);
        holder.moreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(holder.moreOptions.getContext(), holder.moreOptions);
                MenuInflater inflater = popup.getMenuInflater();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.action_edit:
                                return true;
                            case R.id.action_delete:
                                showDeleteDialog(holder.moreOptions.getContext(), h);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                inflater.inflate(R.menu.menu_card, popup.getMenu());
                popup.show();
            }
        });
    }

    public String formatRating(float rating) {
        return String.valueOf((int) rating);
    }

    private View.OnClickListener getImageListener(final String path) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), VideoPlayerActivity.class);
                intent.putExtra("path", path);
                view.getContext().startActivity(intent);
            }
        };
    }

    private String getMediaPath(UUID id, String append) {
        return new File(HaircutIO.dir, String.format("%s-%s.mp4", id.toString(), append)).getPath();
    }

    private void showDeleteDialog(Context context, final Haircut haircut) {
        new MaterialDialog.Builder(context)
                .title(R.string.dialog_are_you_sure)
                .content(R.string.dialog_delete_content)
                .positiveText(R.string.delete)
                .negativeText(android.R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        delete(haircut);
                    }
                })
                .show();
    }

    private void delete(Haircut h) {
        haircuts.remove(h);
        HaircutIO.save(haircuts);
        notifyDataSetChanged();
        HaircutIO.deleteMedia(h.getID());
    }

    @Override
    public int getItemCount() {
        return haircuts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView before;
        ImageView after;
        TextView date;
        TextView hairdresser;
        TextView price;
        TextView currencyType;
        TextView ratingSeparator;
        TextView rating;
        ImageButton moreOptions;


        public ViewHolder(View v) {
            super(v);
            before = v.findViewById(R.id.before_image);
            after = v.findViewById(R.id.after_image);
            date = v.findViewById(R.id.date);
            hairdresser = v.findViewById(R.id.hairdresser);
            price = v.findViewById(R.id.price);
            currencyType = v.findViewById(R.id.currency_type);
            ratingSeparator = v.findViewById(R.id.separator2);
            rating = v.findViewById(R.id.rating);
            moreOptions = v.findViewById(R.id.more_options);
        }
    }
}

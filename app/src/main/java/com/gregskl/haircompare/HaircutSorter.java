package com.gregskl.haircompare;

import android.support.v7.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HaircutSorter {

    private List<Haircut> haircuts;
    private RecyclerView.Adapter adapter;

    public HaircutSorter(List<Haircut> haircuts, RecyclerView.Adapter adapter) {
        this.haircuts = haircuts;
        this.adapter = adapter;
    }

    public void sortByDate(final boolean ascending) {
        Collections.sort(haircuts, new Comparator<Haircut>() {
            @Override
            public int compare(Haircut h1, Haircut h2) {
                if(ascending)
                    return h1.getDate().compareTo(h2.getDate());
                else
                    return h2.getDate().compareTo(h1.getDate());
            }
        });
        adapter.notifyDataSetChanged();
    }

    public void sortByHairdresser(final boolean ascending) {
        Collections.sort(haircuts, new Comparator<Haircut>() {
            @Override
            public int compare(Haircut h1, Haircut h2) {
                if(ascending)
                    return h1.getHairdresser().compareTo(h2.getHairdresser());
                else
                    return h2.getHairdresser().compareTo(h1.getHairdresser());
            }
        });
        adapter.notifyDataSetChanged();
    }

    public void sortByPrice(final boolean ascending) {
        Collections.sort(haircuts, new Comparator<Haircut>() {
            @Override
            public int compare(Haircut h1, Haircut h2) {
                if (ascending)
                    return Integer.compare(h1.getPrice(), h2.getPrice());
                else
                    return Integer.compare(h2.getPrice(), h1.getPrice());
            }
        });
        adapter.notifyDataSetChanged();
    }
}

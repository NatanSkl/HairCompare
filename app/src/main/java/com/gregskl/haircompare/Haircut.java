package com.gregskl.haircompare;

import java.util.Date;
import java.util.UUID;

public class Haircut {

    private Date date;
    private String hairdresser;
    private int price;
    private UUID id;
    private String currencyType;
    private float rating;

    public Haircut(Date date, String hairdresser, int price, UUID id, String currencyType, float rating) {
        this.date = date;
        this.hairdresser = hairdresser;
        this.price = price;
        this.id = id;
        this.currencyType = currencyType;
        this.rating = rating;
    }

    public Haircut(Date date, String hairdresser, int price, String currencyType, float rating) {
        this(date, hairdresser, price, UUID.randomUUID(), currencyType, rating);
    }

    public Date getDate() {
        return date;
    }

    public String getHairdresser() {
        return hairdresser;
    }

    public int getPrice() {
        return price;
    }

    public UUID getID() {
        return id;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public float getRating() {
        return rating;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setHairdresser(String hairdresser) {
        this.hairdresser = hairdresser;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setID(UUID id) {
        this.id = id;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}

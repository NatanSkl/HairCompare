package com.gregskl.haircompare.addhaircut;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;

import com.gregskl.haircompare.R;
import com.gregskl.haircompare.settings.SettingsActivity;

public class HaircutAddActivity extends AppCompatActivity {

    private MediaAddFragment beforeFragment;
    private MediaAddFragment afterFragment;
    private RatingBar ratingBar;
    private EditText hairdresserEdit;
    private EditText priceEdit;
    private EditText currencyTypeEdit;

    private boolean created = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haircut_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        beforeFragment = (MediaAddFragment) getSupportFragmentManager().findFragmentById(R.id.before_fragment);
        afterFragment = (MediaAddFragment) getSupportFragmentManager().findFragmentById(R.id.after_fragment);
        ratingBar = findViewById(R.id.rating);
        hairdresserEdit = findViewById(R.id.hairdresser);
        priceEdit = findViewById(R.id.price);
        currencyTypeEdit = findViewById(R.id.currency_type);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        currencyTypeEdit.setText(prefs.getString("currency_type", getResources().getString(R.string.pref_default_currency)));

        findViewById(R.id.linear_vertical).requestFocus();
        initCreateButton();
    }

    private void initCreateButton() {
        Button createButton = (Button) findViewById(R.id.create_button);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean canCreate = true;
                if(beforeFragment.getMediaPath() == null || afterFragment.getMediaPath() == null) {
                    canCreate = false;
                    LinearLayout linearLayout = findViewById(R.id.linear_vertical);
                    Snackbar snackbar = Snackbar.make(linearLayout, R.string.error_haircut_recording_incomplete, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
                if(hairdresserEdit.getText().toString().equals("")) {
                    canCreate = false;
                    hairdresserEdit.setError(getString(R.string.error_empty_field));
                }
                if(priceEdit.getText().toString().equals("")) {
                    canCreate = false;
                    priceEdit.setError(getString(R.string.error_empty_field));
                }
                if(!SettingsActivity.isCurrencySymbol(currencyTypeEdit.getText().toString())) {
                    canCreate = false;
                    currencyTypeEdit.setError(getString(R.string.error_invalid_currency_symbol));
                }
                if(canCreate) {
                    created = true;
                    Intent data = new Intent();
                    data.putExtra("beforePath", beforeFragment.getMediaPath());
                    data.putExtra("afterPath", afterFragment.getMediaPath());
                    data.putExtra("hairdresser", hairdresserEdit.getText().toString().trim());
                    data.putExtra("price", Integer.valueOf(priceEdit.getText().toString()));
                    data.putExtra("currency_type", currencyTypeEdit.getText().toString());
                    data.putExtra("rating", ratingBar.getRating());
                    setResult(RESULT_OK, data);
                    finish();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);

        String hairdresser = prefs.getString("hairdresser", "");
        hairdresserEdit.setText(hairdresser);

        String priceString = prefs.getString("price", "");
        priceEdit.setText(priceString);
        SharedPreferences generalPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String currencyType = prefs.getString("currency_type",
                generalPrefs.getString("currency_type", getResources().getString(R.string.pref_default_currency)));
        currencyTypeEdit.setText(currencyType);
        float rating = prefs.getFloat("rating", 0.0f);
        ratingBar.setRating(rating);
        String beforePath = prefs.getString("beforePath", null);
        if(beforePath != null) {
            beforeFragment.setMediaPath(beforePath);
            beforeFragment.initMediaView();
        }
        String afterPath = prefs.getString("afterPath", null);
        if(afterPath != null) {
            afterFragment.setMediaPath(afterPath);
            afterFragment.initMediaView();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        if(created) {
            editor.clear();
        }
        else {
            editor.putString("hairdresser", hairdresserEdit.getText().toString());
            editor.putString("price", priceEdit.getText().toString());
            editor.putString("currency_type", currencyTypeEdit.getText().toString());
            editor.putFloat("rating", ratingBar.getRating());
            editor.putString("beforePath", beforeFragment.getMediaPath());
            editor.putString("afterPath", afterFragment.getMediaPath());
        }
        editor.apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

